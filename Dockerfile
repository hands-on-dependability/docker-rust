FROM debian:bookworm-slim

RUN apt update && apt install -y build-essential curl make libssl-dev pkg-config

# Rustup
ENV RUSTUP_HOME=/opt/rustup
ENV CARGO_HOME=/opt/cargo
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
ENV PATH="${PATH}:/opt/cargo/bin"

# Rust Toolchain
ENV RUST_TOOLCHAIN="stable"
RUN rustup toolchain install "${RUST_TOOLCHAIN}"
RUN rustup toolchain install "nightly"
RUN rustup default "${RUST_TOOLCHAIN}"

# Rust Tools
RUN rustup component add clippy
RUN rustup component add rustfmt
RUN cargo install cargo-about \
    cargo-outdated \
    cargo-udeps \
    cargo-tarpaulin

# Docker + Cross
RUN apt install -y apt-transport-https \
    ca-certificates \
    gnupg-agent \
    software-properties-common && \
    rm -rf /var/lib/apt/lists/*
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
RUN chmod a+r /etc/apt/keyrings/docker.gpg
RUN echo \
    "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
    "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
    tee /etc/apt/sources.list.d/docker.list > /dev/null
RUN apt update && \
    apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
RUN cargo install cross

# Modify cross tool to be able to access rustup and cargo from spawned container
RUN mv /opt/cargo/bin/cross /opt/cargo/bin/real-cross
COPY cross /opt/cargo/bin/cross
RUN chmod +x /opt/cargo/bin/cross

# Utils
RUN apt install -y zip && \
    rm -rf /var/lib/apt/lists/*
